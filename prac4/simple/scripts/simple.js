function doIt(){
    
    let num1Ref = document.getElementById("number1")
    let num2Ref = document.getElementById("number2")
    let num3Ref = document.getElementById("number3")
    let answer = parseInt(num1Ref.value) + parseInt(num2Ref.value) + parseInt(num3Ref.value)
    
    let answerRef = document.getElementById("answer")
    answerRef.innerHTML = answer;
    
    if(answer >= 0){
       answerRef.className = "positive"
    }else{
        answerRef.className = "negative"
    }
    
    let oddEvenRef = document.getElementById("oddEven")
    
    if(answer % 2 === 0){
        oddEvenRef.innerHTML = "(even)";
       oddEvenRef.className = "even"
    }else{
        oddEvenRef.innerHTML = "(odd)";
        oddEvenRef.className = "odd"
    
}
}